import { CounterPage } from './app.po';
import { ElementFinder } from "protractor/built";

const clickNTimes = async (n: number, ef: ElementFinder) => {
  for(let i = 0; i < n; ++i) {
    await ef.click()
  }
}

describe('counter App', () => {
  let page: CounterPage;

  beforeEach(() => {
    page = new CounterPage();
  });

  it('should increase counter', async () => {
    await page.navigateTo();
    await clickNTimes(1, page.getIncreaseBtn())
    const couterValue = await page.getCounterValue()
    const couterColor = await page.getCounterValueColor()
    expect(couterValue).toEqual('2')
    expect(couterColor).toEqual('rgba(0, 0, 0, 1)')
  });

  it('should be "Fizz"', async () => {
    await page.navigateTo();
    await clickNTimes(2, page.getIncreaseBtn())
    const couterValue = await page.getCounterValue()
    const couterColor = await page.getCounterValueColor()
    expect(couterValue).toEqual('Fizz')
    expect(couterColor).toEqual('rgba(255, 255, 0, 1)')
  });

  it('should be "Buzz"', async () => {
    await page.navigateTo();
    await clickNTimes(4, page.getIncreaseBtn())
    const couterValue = await page.getCounterValue()
    const couterColor = await page.getCounterValueColor()
    expect(couterValue).toEqual('Buzz')
    expect(couterColor).toEqual('rgba(0, 0, 255, 1)')
  });

  it('should be "FizzBuzz"', async () => {
    await page.navigateTo();
    await clickNTimes(14, page.getIncreaseBtn())
    const couterValue = await page.getCounterValue()
    const couterColor = await page.getCounterValueColor()
    expect(couterValue).toEqual('FizzBuzz')
    expect(couterColor).toEqual('rgba(0, 128, 0, 1)')
  });
});
