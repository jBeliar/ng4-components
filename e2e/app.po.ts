import { browser, by, element } from 'protractor';

export class CounterPage {
  private getCounter() {
    return element(by.css('.c-container span'))
  }

  navigateTo() {
    return browser.get('/');
  }

  getIncreaseBtn() {
    return element(by.css('.c-container button'));
  }

  getCounterValue() {
    return this.getCounter().getText()
  }

  getCounterValueColor() {
    return this.getCounter().getCssValue("color")
  }
}
