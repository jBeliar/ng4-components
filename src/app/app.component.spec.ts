import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { CounterComponent } from './counter/counter.component';
import { UploaderComponent } from './uploader/uploader.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CounterComponent,
        UploaderComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
