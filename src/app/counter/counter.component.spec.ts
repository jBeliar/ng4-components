import { TestBed, async } from '@angular/core/testing';

import { CounterComponent } from './counter.component';
import * as utils from './counter.utils'

describe('CounterComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CounterComponent
      ],
    }).compileComponents();
  }));

  it('should create the counter', async(() => {
    const fixture = TestBed.createComponent(CounterComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  describe('Utils', () => {
    it('should return computed value', () => {
      expect(utils.computeValue(3)).toBe('Fizz');
      expect(utils.computeValue(5)).toBe('Buzz');
      expect(utils.computeValue(7)).toBe('7');
      expect(utils.computeValue(15)).toBe('FizzBuzz');
    });

    it('should return class names', () => {
      expect(utils.getCounterClass('Fizz')).toEqual({
        'c-counter--fizz': true,
        'c-counter--buzz': false,
        'c-counter--fizzbuzz': false,
      });
      expect(utils.getCounterClass('Buzz')).toEqual({
        'c-counter--fizz': false,
        'c-counter--buzz': true,
        'c-counter--fizzbuzz': false,
      });
      expect(utils.getCounterClass('FizzBuzz')).toEqual({
        'c-counter--fizz': false,
        'c-counter--buzz': false,
        'c-counter--fizzbuzz': true,
      });
      expect(utils.getCounterClass('7')).toEqual({
        'c-counter--fizz': false,
        'c-counter--buzz': false,
        'c-counter--fizzbuzz': false,
      });
    });
  })

  
});
