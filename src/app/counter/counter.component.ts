import { Component } from '@angular/core';
import * as utils from './counter.utils';

@Component({
  selector: 'counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.css']
})
export class CounterComponent {

  counter: number
  value: string

  constructor() {
    this.counter = 1
    this.value = utils.computeValue(this.counter)
  }

  get counterClass() {
    return utils.getCounterClass(this.value)
  }

  increase() {
    this.counter += 1
    this.value = utils.computeValue(this.counter)
  }
}


