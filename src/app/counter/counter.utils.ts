export const computeValue = (counter: number) => {
  let value = ''
  if (counter % 3 === 0) {
    value = 'Fizz'
  }
  if (counter % 5 === 0) {
    value += 'Buzz'
    return value
  }
  if(value === '') {
    value += counter
  }
  return value
}

export const getCounterClass = (value: string) => {
  return {
    'c-counter--fizz': value == 'Fizz',
    'c-counter--buzz': value == 'Buzz',
    'c-counter--fizzbuzz': value == 'FizzBuzz',
  }
}