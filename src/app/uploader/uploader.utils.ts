export const parseSize = (size: number) => {
  
  const kSize = Math.floor(size / 1024)
  const MSize = Math.floor(kSize / 1024)

  if (MSize) {
    return MSize + ' MB'
  } else if (kSize) {
    return kSize + ' kB'
  } else {
    return size + ' B'
  }
}