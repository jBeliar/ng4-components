import { Component } from '@angular/core';

import * as utils from './uploader.utils'

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.css']
})
export class UploaderComponent {
  name: string
  mime: string
  width: number
  height: number
  size: string
  filePath: any

  onFileChange(event: any): void {
    const file = event.target.files[0]
    if(!file) {
      return
    }
    this.name = file.name
    this.size = utils.parseSize(file.size)
    this.mime = file.type

    if(file.type.split('/')[0] == 'image') {
      this.loadImage(file)
    } else {
      this.width = 0
      this.height = 0
      this.filePath = ''
    }
  }

  private loadImage(file) {
    const reader = new FileReader()
    const image = new Image()
    reader.onloadend = (e: any) => {
      const src = e.target.result
      this.filePath = src
      image.onload = e => {
        this.width = image.width
        this.height = image.height
      }
      image.src = reader.result
    };
    reader.readAsDataURL(file)
  }
}




