import { TestBed, async } from '@angular/core/testing';

import { UploaderComponent } from './uploader.component';
import * as utils from './uploader.utils'

describe('UploaderComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UploaderComponent
      ],
    }).compileComponents();
  }));

  it('should create the uploader', async(() => {
    const fixture = TestBed.createComponent(UploaderComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  describe('Utils', () => {
    it('should return parse size', () => {
      expect(utils.parseSize(123)).toBe('123 B')
      expect(utils.parseSize(1024)).toBe('1 kB')
      expect(utils.parseSize(6666666)).toBe('6 MB')
    })
  })
});
